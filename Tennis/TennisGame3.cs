using System;

namespace Tennis
{
    public class TennisGame3 : ITennisGame
    {
        static readonly string[] PointValues = { "Love", "Fifteen", "Thirty", "Forty" };

        private readonly Player player1;
        private readonly Player player2;

        public TennisGame3(string player1Name, string player2Name)
        {
            player1 = new Player(player1Name);
            player2 = new Player(player2Name);
        }

        bool NormalDisplay => player1.Score < 4 && player2.Score < 4 && (player1.Score + player2.Score < 6);
        string LeadingPlayerName => player1.Score > player2.Score ? player1.Name : player2.Name;
        bool IsAdvantage => Math.Abs(player1.Score - player2.Score) == 1;

        public string GetScore()
        {
            if (NormalDisplay)
                return $"{PointValues[player1.Score]}-{GetSecondPlayerScoreText()}";
            
            if (player1.Score == player2.Score)
                return "Deuce";

            string leadingText = IsAdvantage ? "Advantage" : "Win for";
            return $"{leadingText} {LeadingPlayerName}";
        }

        string GetSecondPlayerScoreText()
        {
            if (player1.Score == player2.Score)
                return "All";
            return PointValues[player2.Score];
        }


        public void WonPoint(string playerName)
        {
            if (playerName == "player1")
                player1.Score += 1;
            else
                player2.Score += 1;
        }

    }
}

