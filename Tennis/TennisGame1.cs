namespace Tennis
{
    class TennisGame1 : ITennisGame
    {
        private int scorePlayer1 = 0;
        private int scorePlayer2 = 0;

        public TennisGame1(string player1Name, string player2Name)
        {
        }

        public void WonPoint(string playerName)
        {
            if (playerName == "player1")
                scorePlayer1 += 1;
            else
                scorePlayer2 += 1;
        }

        public string GetScore()
        {
            if (scorePlayer1 == scorePlayer2)
                return GetScoreEqual();

            if (scorePlayer1 >= 4 || scorePlayer2 >= 4)
                return GetScoreAdvantageOrWin();

            return $"{ScoreHelper.ScoreToText(scorePlayer1)}-{ScoreHelper.ScoreToText(scorePlayer2)}";
        }

        private string GetScoreAdvantageOrWin()
        {
            int scoreDifference = scorePlayer1 - scorePlayer2;

            if (scoreDifference == 1) 
                return "Advantage player1";
            if (scoreDifference == -1) 
                return "Advantage player2";
            if (scoreDifference >= 2) 
                return "Win for player1";

            return "Win for player2";
        }

        private string GetScoreEqual()
        {
            if (scorePlayer1 < 3)
                return $"{ScoreHelper.ScoreToText(scorePlayer1)}-All";
            return "Deuce";        
        }
    }
}

