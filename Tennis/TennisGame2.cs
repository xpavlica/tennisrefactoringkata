namespace Tennis
{
    public class TennisGame2 : ITennisGame
    {
        private int scorePlayer1;
        private int scorePlayer2;

        public TennisGame2(string player1Name, string player2Name)
        {
        }

        public string GetScore()
        {
            if (scorePlayer1 >= 4 && scorePlayer2 >= 0 && (scorePlayer1 - scorePlayer2) >= 2)
            {
                return "Win for player1";
            }
            if (scorePlayer2 >= 4 && scorePlayer1 >= 0 && (scorePlayer2 - scorePlayer1) >= 2)
            {
                return "Win for player2";
            }

            if (scorePlayer1 > scorePlayer2 && scorePlayer2 >= 3)
            {
                return "Advantage player1";
            }

            if (scorePlayer2 > scorePlayer1 && scorePlayer1 >= 3)
            {
                return "Advantage player2";
            }


            if (scorePlayer1 == scorePlayer2)
            {
                if (scorePlayer1 < 3)
                    return ScoreHelper.ScoreToText(scorePlayer1) + "-All";
                if (scorePlayer1 > 2)
                    return "Deuce";
            }


            if (scorePlayer1 > 0 && scorePlayer2 == 0)
            {
                return ScoreHelper.ScoreToText(scorePlayer1) + "-Love";
            }
            if (scorePlayer2 > 0 && scorePlayer1 == 0)
            {
                return "Love-" + ScoreHelper.ScoreToText(scorePlayer2);
            }

            if (scorePlayer1 > scorePlayer2 && scorePlayer1 < 4 || scorePlayer2 > scorePlayer1 && scorePlayer2 < 4)
            {
                return ScoreHelper.ScoreToText(scorePlayer1) + "-" + ScoreHelper.ScoreToText(scorePlayer2);
            }

            return string.Empty;
        }

        public void WonPoint(string playerName)
        {
            if (playerName == "player1")
                scorePlayer1++;
            else
                scorePlayer2++;
        }
    }
}

